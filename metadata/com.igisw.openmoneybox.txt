Categories:Money
License:GPL-3.0-only
Web Site:http://igisw-bilancio.sourceforge.net/
Source Code:https://launchpad.net/bilancio
Issue Tracker:https://bugs.launchpad.net/bilancio
Changelog:https://sourceforge.net/p/igisw-bilancio/wiki/Changelog_3.2.1_Android/
Donate:http://igisw-bilancio.sourceforge.net/donation.html

Auto Name:OpenMoneyBox

Repo Type:bzr
Repo:lp:bilancio/trunk

Build:3.1.2.5,17
    commit=4
    subdir=android/API24
    gradle=yes

Build:3.2.1.1,18
    commit=14
    subdir=android/API24
    gradle=yes

Maintainer Notes:
Summary and Description have been moved to the new localizable text files:
https://f-droid.org/docs/All_About_Descriptions_Graphics_and_Screenshots
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.2.1.1
Current Version Code:18
